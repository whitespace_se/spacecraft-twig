'use strict';

module.exports = function(fractal){

    return {
       templateContext: require('./templateContext.js')(fractal),
       templateData: require('./templateData.js')(fractal),
       t: require('./t.js')(fractal),
       trans: require('./trans.js')(fractal),
       route: require('./route.js')(fractal),
       url: require('./url.js')(fractal),
       function: require('./function.js')(fractal)       
    }

};
