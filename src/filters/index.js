'use strict';

module.exports = function(fractal){

    return {
        path: require('./path.js')(fractal),
        resize: require('./resize.js')(fractal),
        wpautop: require('./wpautop.js')(fractal)        
    }

};
